/**
 * @format
 */
import React from 'react'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// redux
import logger from 'redux-logger'
import reducer from './src/Redux/Reducers'
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';

const store = createStore(reducer,applyMiddleware(logger))

const AppContainer = () => (
  <Provider store={store}>
    <App />
  </Provider>
);


AppRegistry.registerComponent(appName, () => AppContainer);
