import React from 'react';
import {
  StatusBar,
} from 'react-native';
import TodoScreen from './src/Containers'


const App = () => {
  return (
    <>
      <StatusBar backgroundColor="#EB5757" barStyle="light-content" />
      <TodoScreen/> 
    </>
  );
};



export default App;
