export const addTodo = val => {
  return {
    type: 'ADD_TODO',
    payload: val,
  };
};
export const editTodo = val => {
  return {
    type: 'EDIT_TODO',
    payload: val,
  };
};

export const deleteTodo = val =>{
    return{
        type: 'DELETE_TODO',
        payload: val,
    }
}
export const handleTodo = val =>{
    return{
        type: 'HANDLE_TODO',
        payload: val,
    }
}