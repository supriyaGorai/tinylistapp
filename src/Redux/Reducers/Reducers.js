const initialState = {
  todo: []
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      state.todo.unshift(action.payload);
      return {
        ...state,
      };
    case 'EDIT_TODO':
      const todoArr = [...state.todo]
      const id = todoArr.findIndex(item => item.id === action.payload.itemId);
      console.log("new title", action.payload.newValue);
      console.log("old title", state.todo[id]);
      state.todo[id].title = action.payload.newValue;
      return {
        ...state,
        // todo: action.payload,
      };
    case 'DELETE_TODO':
      const newTodo = [...state.todo];
      const index = newTodo.findIndex(item => item.id === action.payload);
      newTodo.splice(index, 1);
      return {
        ...state,
        todo: newTodo
      };
    case 'HANDLE_TODO':
      const newArr = [...state.todo];
      const i = newArr.findIndex(item => item.id === action.payload);
      const item = newArr[i]
      newArr.splice(i, 1);
      newArr.push(item)
      return {
        ...state,
        todo: newArr,
      };
    default:
      return state;
  }
};

export default reducers;