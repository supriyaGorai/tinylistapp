import React, { useState } from "react";
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet , 
    Modal, 
    Alert, 
    Dimensions, 
    TextInput,
    TouchableWithoutFeedback,
    Animated,
    Easing
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CheckBox from '@react-native-community/checkbox';

import {connect} from 'react-redux';
import {deleteTodo, handleTodo, editTodo} from './../Redux/Actions/Actions';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

 const ListComponent = (props) =>  {
    const [isChecked, setIsChecked] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [listId, setListId] = useState("");
    const ref = React.useRef(View.prototype);
    const animatedValue = React.useRef(new Animated.Value(0)).current;

    const [textWidth, setTextWidth] = React.useState(0);
    const [textHeight, setTextHeight] = React.useState(0);

    const handleDelete=(id)=>{
        props.deleteTodo(id)
    }
    const handleCheckBox = id => {
        if (!isChecked) {
        animatedValue.setValue(0);
        } 
        setIsChecked(!isChecked)
        if(ref.current){
            ref.current.measure((x, y, w, h) => { 
            setTextWidth(w);
            setTextHeight(h);
            animateStrike();
        });
        }
    }
    const handleCloseModal = ()=>{
        setInputValue("")
        setListId("")
        setIsModalOpen(false)
    }
    const handleEdit = value => {
        setInputValue(value.title)
        setListId(value.id)
        setIsModalOpen(true)
    }
    const onPressEdit = (id)=>{
        if(inputValue !== ""){
            setIsModalOpen(false)
            const item = {
                itemId:listId,
                newValue: inputValue
            }
            props.editTodo(item)
            setInputValue("")
            setListId("")
            setIsModalOpen(false)
        }
        
    }
    

  const animateStrike = () => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 800,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();
  };

  const strikeWidth = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, textWidth],
    extrapolate: 'clamp',
  });

    return (
        <View>
        <View style={styles.listContainer}>
            <TouchableOpacity onPress={()=>handleEdit(props.data)} style={styles.listItem}>  
                <CheckBox
                onFillColor="#EB5757"
                value={isChecked}
                onValueChange={() => handleCheckBox(props.data.id)}
                />
                <View >
                    <Text 
                        ref={ref}
                        style={[styles.listItemText,{color: isChecked ? "#979797" : "#222"}]}
                        >{props.data.title}</Text>
                        
                        {isChecked ? 
                        <Animated.View
                            style={{ 
                                position: 'absolute',
                                height: 2,
                                backgroundColor: '#979797',
                                width: strikeWidth, 
                                top: textHeight / 2 + 1 ,
                                zIndex:20

                            }}
                        />
                        : null }
                  </View>
                
            </TouchableOpacity>
            
            <MaterialIcons onPress={()=>handleDelete(props.data.id)} name="delete" size={30} color="#979797" />
            
        </View>
            <Modal
            animationType="fade"
            transparent={true}
            visible={isModalOpen}
            
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            }}
            >
            <TouchableWithoutFeedback onPress={handleCloseModal}>
            <View  style={styles.editListContainer}>
            <View style={styles.editLiatView}>
                <TextInput
                style={styles.inputStyle}
                value={inputValue}
                onChangeText={ val => setInputValue(val)}
                />

                <TouchableOpacity
                style={styles.sendButton}
                onPress={onPressEdit}
                >
                    <Ionicons name="send" size={30} color="#fff"/>
                </TouchableOpacity>
            </View>
            </View>
            </TouchableWithoutFeedback>
        </Modal>
        </View>
    )
}


const styles = StyleSheet.create({
    listContainer:{
        flexDirection:'row',
        marginHorizontal:20,
        marginVertical:20 ,
    },
    listItem:{
        flexDirection:'row',
        alignItems:'center',
        width:'90%',
    },
    listItemText:{
        color:'#222',
        fontSize:20,
        paddingLeft:10,
    },
    editListContainer: {
    flex: 1,
    backgroundColor:"rgba(0,0,0,0.4)",
    justifyContent: 'flex-end',
  },
  editLiatView: {
    width:screenWidth,
    backgroundColor: "#EB5757",
    borderTopRightRadius:20,
    borderTopLeftRadius:20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    paddingVertical:20,
  },
  inputStyle:{
      width:'80%',
      backgroundColor:'#fff',
      borderRadius:20,
      fontSize:20,
      paddingHorizontal:10
  },
  sendButton: {
    padding: 10,
  },
  
});

const mapStateToProps = state => ({
  todos: state.todo
});
const mapDispatchToProps =  {
   deleteTodo , 
   handleTodo,
   editTodo
}

export default connect (mapStateToProps,mapDispatchToProps)(ListComponent);
