import {StyleSheet, Dimensions} from 'react-native'
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#fff',
        
    },
    //   header
    header:{
      width: screenWidth,
      height: screenHeight/10,
      backgroundColor:'#EB5757',
      justifyContent:'center',
      alignItems:'center'
  },
    headerText:{
        color:'#fff',
        fontSize:25,
        fontWeight:'bold',
    },
    // input
    inputContainer:{
        flexDirection:'row',
        alignItems:'center',
        borderBottomWidth:1,
        borderBottomColor:'#ccc',
        marginHorizontal:20,
        marginVertical:20 
    },
    input:{
        fontSize:20,
        width:'100%',
        paddingLeft:10
    },
    // list
    listContainer:{
        flexDirection:'row',
        alignItems:'center',
        // justifyContent:'space-between',
        marginHorizontal:20,
        marginVertical:20 
    },
    listItem:{
        flexDirection:'row',
        alignItems:'center',
        width:'90%'
    },
    listItemText:{
        color:'#222',
        fontSize:20,
        paddingLeft:10,
        
    },
  
});

export default styles;