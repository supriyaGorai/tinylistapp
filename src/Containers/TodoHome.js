import React, { Component } from 'react'
import { 
    Text,
    View, 
    FlatList, 
    TextInput, 
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity
 } from 'react-native'
import styles from './Styles'
import Entypo from 'react-native-vector-icons/Entypo';
import {addTodo} from './../Redux/Actions/Actions';
import {connect} from 'react-redux'
import Lists from './../Components/ListComponent'


class TodoHome extends Component {
    constructor(props){
        super(props);
        this.state={
            inputValue:'',
            isChecked: false
        }
    }
    handleSubmit=()=>{
        if(this.state.inputValue !== ''){
            const newItem = {
                id: Date.now(),
                title: this.state.inputValue
                };
                this.props.addTodo(newItem);
                this.setState({inputValue:''})
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
                <View style={styles.container}>
                    {/* header */}
                    <View style={styles.header}>
                        <Text style={styles.headerText}>
                            TinyList
                        </Text>
                    </View>
                    {/* input */}
                    <View style={styles.inputContainer}>
                        <Entypo name="plus" size={30} color="#EB5757" />
                        <TextInput
                        style={styles.input}
                        placeholder="Add to list..."
                        placeholderTextColor='#EB5757'
                        value={this.state.inputValue}
                        onChangeText={(val)=>this.setState({inputValue:val})}
                        onSubmitEditing={this.handleSubmit}
                        />
                    </View>
                    {/* list */}
                    <FlatList
                        data={this.props.todos}
                        renderItem={({item}) => {
                            return(
                                <Lists data={item}/>
                            )
                        }}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProps = state => ({
  todos: state.todo
});
const mapDispatchToProps = {
 addTodo,
};

export default connect(mapStateToProps,mapDispatchToProps)(TodoHome)